import React, {useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import classes from './Edit.module.css'
import {editData} from "../action/action";

export const Edit = () => {

    const DISPATCH = useDispatch();
    const OBJECT = useSelector(store => store.appReducer.object);
    const EDIT_CLASSES = {
        SUBMIT: classes.submit
    }
    const TEXT_INPUT = useRef()

    const Edit_Data = (key, value) =>{
        console.log(key,value)
        DISPATCH(editData( key,  value ))
    }

    return(
        OBJECT.map(element => {
            if(element.flag === true) {
                return (
                    <div key={element.key}>
                        <form>
                            <input ref = {TEXT_INPUT} defaultValue={element.title}/>
                            <button className={EDIT_CLASSES.SUBMIT} onClick={(event) => {
                                event.preventDefault()
                                Edit_Data(element.key, TEXT_INPUT.current.value)
                            }}>Edit</button>
                        </form>
                    </div>
                )
            }
        })
    )
}