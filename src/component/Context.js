import React, {Fragment, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteData, editDataFlag, setActive,} from "../action/action";
import {Edit} from "./Edit";
import classes from "./Context.module.css"

const TODO_CLASSES = {
        ACTIVE: classes.active2,
        BLOCK: classes.block,
        BUTTONS: classes.buttons,
        EDIT: classes.edit,
        DELETE: classes.delete
}

export const Context = () => {
    const DISPATCH = useDispatch();
    const OBJECT = useSelector(store => store.appReducer.object);
    const KEY  = useSelector(store=>store.appReducer.key);
    let Old_Key = useRef();
        Old_Key = KEY
    let edit;
    let active;

    const setActiveKey = (key,oldKey) => {
       return  DISPATCH(setActive(key, oldKey))
    }

    const editFlag = (key) => {
        DISPATCH(editDataFlag(key));
    }

    const deleteTodo = (key) => {
        DISPATCH(deleteData(key))
    }
    return (

        OBJECT.map(elem => {
            if ( elem.key === KEY) {
                active = TODO_CLASSES.ACTIVE
            }
            else {
                active = TODO_CLASSES.BLOCK
            }

            elem.flag ? edit = true : edit = false

                return (
                    <Fragment key={elem.key}>
                        <div  className={active} onClick={() => setActiveKey(elem.key, Old_Key)}>
                            <div>
                                <strong>{elem.title}</strong>
                                <small>{elem.date}</small>
                            </div>
                            {/*<div className={TODO_CLASSES.BUTTONS} onClick={(Event) => Event.stopPropagation()}>*/}
                            <div className={TODO_CLASSES.BUTTONS}>
                                <button className={TODO_CLASSES.EDIT} onClick={ () => editFlag(elem.key)}>edit</button>
                                <button className={TODO_CLASSES.DELETE} onClick={() => deleteTodo(elem.key)}>delete</button>
                            </div>
                        </div>
                            {edit && <Edit/>}
                    </Fragment>
                )

            }
        )
    )
}
