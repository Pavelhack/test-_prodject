import React, {useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setConsole, setData} from "../action/action";


export const Form = () => {
    const DISPATCH = useDispatch();
    const OBJECT = useSelector(store => store.appReducer.object)
    const VALUE_INPUT = useRef();

    const add = (titleData) => {
        DISPATCH(setData({key: OBJECT.length + 1, title: titleData, date: new Date().toDateString(), flag: false} ))
        VALUE_INPUT.current.value = '';
    }

    const handleChange = (event) => {
        //DISPATCH(setConsole(event.target.value))
        DISPATCH(setConsole(VALUE_INPUT.current.value))
    }

    return (
        <form>
            <input type="text"  className={"input_title"} onChange = {handleChange} placeholder="Ваша запись"/>
            <button className={"submit"} onClick={(event) => {
                event.preventDefault()
                add(VALUE_INPUT.current.value)
            }} >отправить
            </button>
        </form>
    )

}