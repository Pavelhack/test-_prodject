import React from 'react';
import {Form} from './Form';
import {Context} from "./Context";
import classes from './Test.module.css';

export const Test = () => {

    const TEST_CLASSES = {
        TEST_BLOCK: classes.test_block
    }

    return (
            <div className= {TEST_CLASSES.TEST_BLOCK}>
                <Form/>
                <Context/>
            </div>
    );
}