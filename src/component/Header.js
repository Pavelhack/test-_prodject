import React from 'react';
import {NavLink} from "react-router-dom";
import Logo from "../images/logo.jpg";
import classes from './Header.module.css'

export const Header =  () => {
    const HEADER_CLASSES = {
        HEADER: classes.header,
        HEADER_LOGO: classes.header__logo,
        HEADER_NAV_LIST: classes.header__nav_list,
        ACTIVE_LINK: classes.active_link,
        HEADER_NAV: classes.header__nav
    }

    return(
        <header className= {HEADER_CLASSES.HEADER}>
            <div className= {HEADER_CLASSES.HEADER_LOGO}>
                <img src= {Logo} alt="Antalogic"/>
            </div>
            <nav className= {HEADER_CLASSES.HEADER_NAV}>
                <ul className= {HEADER_CLASSES.HEADER_NAV_LIST}>
                    <li><NavLink activeClassName={HEADER_CLASSES.ACTIVE_LINK}  to="/Info">Информация</NavLink></li>
                    <li><NavLink activeClassName={HEADER_CLASSES.ACTIVE_LINK} to="/" exact>Главная</NavLink></li>
                </ul>
            </nav>
        </header>
    )
}
