export const  setData = (array) => {
    return {
        type: 'SET_DATA',
        payloadObject: array
    }
}

export const setActive = (key, oldKey) => {
    return {
        type: 'SET_ACTIVE',
        payloadKey: key,
        payloadOldKey: oldKey
    }
}

export const  editDataFlag = (key) => {
    return {
        type: 'EDIT_FLAG',
        payloadKey: key
    }
}

export const  editData = (key, value) => {
    return {
        type: 'EDIT_DATA',
        payloadKey : key,
        payloadDate : new Date().toDateString(),
        payloadTitle : value,
    }
}

export const setConsole = (value) => {
    return {
        type: 'SET_CONSOLE',
        payloadValue: value
    }
}

export const deleteData = (key, object) => {
    return{
        type: 'DELETE_DATA',
        payloadKey: key,
        payloadObject: object
    }
}



