import {combineReducers, compose, createStore} from 'redux'

import {appReducer} from "./reducer"

const ROOTREDUCER = combineReducers( {
    appReducer : appReducer,
})

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
);

export const store = createStore(ROOTREDUCER,enhancer )

