import array from '../array.json';

const INITIALSTATE = {
    object: array,
    valueConsole: '',
    key: ''
}

export const REDUCER_ACTION_TYPES = {
    SET_ACTIVE: 'SET_ACTIVE',
    SET_DATA:'SET_DATA',
    EDIT_FLAG: 'EDIT_FLAG',
    EDIT_DATA: 'EDIT_DATA',
    SET_CONSOLE: 'SET_CONSOLE',
    DELETE_DATA: 'DELETE_DATA'
}

export const appReducer = (state = INITIALSTATE, action) => {
    switch (action.type) {

        case REDUCER_ACTION_TYPES.SET_ACTIVE:
            // delete old flag from object
            const oldObj = state.object.find((item) => item.key === action.payloadOldKey)
            if(oldObj !== undefined && action.payloadKey !== action.payloadOldKey){
                oldObj.flag = false
            }
            return {
                ...state,
                key: action.payloadKey,
            };

        case REDUCER_ACTION_TYPES.SET_DATA:
            return {
                ...state,
                object: [...state.object, action.payloadObject],
            };

        case REDUCER_ACTION_TYPES.EDIT_FLAG: {
            const tempObj = state.object.find((item) => item.key === action.payloadKey)
            tempObj.flag = !tempObj.flag
            return {
                ...state,
                object: [...state.object]
            };
        }

        case REDUCER_ACTION_TYPES.EDIT_DATA:
            const tempObj = state.object.find((item) => item.key === action.payloadKey)
            tempObj.title = action.payloadTitle
            tempObj.date = action.payloadDate
            return {
                ...state,
                object: [...state.object],
            };

        case REDUCER_ACTION_TYPES.SET_CONSOLE:
            return {
                ...state,
                valueConsole: action.payloadValue
            }

        case REDUCER_ACTION_TYPES.DELETE_DATA:
            const delete_ = state.object.filter((element) =>
                action.payloadKey !== element.key
            )
            return {
                ...state,
                object: [...delete_]
            }

        default:
            return state
    }
}
