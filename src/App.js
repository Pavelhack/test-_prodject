import React from "react"
import  {Switch, Route} from "react-router-dom";
import classes from "./App.module.css"
import {Test} from "./component/Test";
import {Info} from "./component/Info";
import {Header} from "./component/Header";

function App() {

const APP_CLASSES = {
    APP: classes.App
    }

    return(
        <div className={APP_CLASSES.APP}>
            <Header/>
            <Switch>
                <Route path={'/Info'} component={Info} />
                <Route path={'/'}  component={Test} exact/>
            </Switch>
        </div>

    )
}

export default App;